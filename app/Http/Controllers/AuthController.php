<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function datadiri () {
        return view('tugaslaravel.form');
    }

    public function signup (Request $request) {
        $satu = $request->first;
        $dua = $request->last;
        return view('tugaslaravel.finish', compact('satu', 'dua'));
    }
}
