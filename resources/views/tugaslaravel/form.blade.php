@extends('layout.master')

@section('title')
Buat Acccount Baru!
@endsection

@section('judul')
Sign Up Form  
@endsection

@section('isi')
  
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br>
            <input type="text" name="first">
        <br><br>
        <label>Last name:</label><br>
            <input type="text" name="last">
        <br><br>
        <label>Gender:</label><br>
            <input type="radio"> Male<br>
            <input type="radio"> Female<br>
            <input type="radio"> Other<br><br>
        <label>Nationality:</label><br>
        <select>
            <option value="1"> Indonesia</option>
            <option value="2"> Saudi Arabia</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
            <input type="checkbox"> Bahasa Indonesia<br>
            <input type="checkbox"> English<br>
            <input type="checkbox"> Other<br><br>
        <label>Bio:</label><br>
        <textarea cols="30" rows="10"></textarea><br>
            <input type="submit" value="Sign Up">
        <br><br><br><br>
@endsection