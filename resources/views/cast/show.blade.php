@extends('layout.master')

@section('title')
Cast Detail {{$cast->nama}}
@endsection

@section('judul')
Detail
@endsection

@section('isi')

<h2>{{$cast->nama}}</h2>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

@endsection